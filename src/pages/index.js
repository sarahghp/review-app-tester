import React from 'react'
import { Layout } from '../components'
import fun from  './fun.gif'

const Sketches = () => {

  return (
    <Layout>
      <div style={{
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'center',
      }}>
        <h2>
          This is a test app for injecting toolbars.
        </h2>
        <h2> → Check out the <a href='https://gitlab.com/gitlab-org/gitlab-ee/issues/10761'> related issue </a> → </h2>
        <hr />
        <img src={ fun } alt='decorative gif with swirling geometry'/>
      </div>
    </Layout>
  )
}

export default Sketches
